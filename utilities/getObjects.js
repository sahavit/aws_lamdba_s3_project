var express = require('express');
var AWS = require('aws-sdk');
var router = express.Router();  

var awsConfig = { 
    "accessKeyId": process.env.ACCESS_KEY_ID,
    "secretAccessKey": process.env.SECRET_ACCESS_KEY,
    "region": process.env.REGION  
};

var s3 = new AWS.S3(awsConfig);

router.get('/all', async function(req, res){
    var params = {
        Bucket: process.env.BUCKET_NAME
        // Bucket: 'mobiotics.image.resize'
    }
  
    s3.listObjects(params, function(err, data) {
        if (err) {
          console.log("Error", err);
          res.status(400).json({'error': err.message});
        } 
        else if(data.Contents.length===0){
            console.log('No images in bucket');
            res.status(400).json({'error': 'No images existing'});
        }
        else {
          var domain = process.env.DOMAIN_NAME;
            // var domain = 'http://d22uq8fpd1rls9.cloudfront.net'
            data.Contents.forEach( (element) => {
                element.ObjectUrl=element.Key; 
                element.Key=domain+'/'+element.Key;             
            })
            res.status(200).send(data.Contents);
        }
    });
  });

  module.exports=router;