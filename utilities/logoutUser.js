var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
var redis = require('../common/redisCache')

router.get('/logout', function(req,res){
    var token = req.headers.authorization.split(' ')[1];
    var decoded = jwt.decode(token, {complete: true})
    redis.deleteCacheStorage(decoded.payload.username).then( (deleted) => {
        if(deleted){
            res.status(200).send({message: 'Sucessfully logged out'})
        }
        else{
            res.status(200).send({message: 'You have already logged out'})
        }
    }).catch( (err) => {
        res.status(400).send({'error': err.message})
    })
})

module.exports=router;