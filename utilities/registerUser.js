var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var generateJWTToken = require('../common/generateJWTToken');
var express = require('express');
var router = express.Router();
var redis = require('../common/redisCache');
const { Validator } = require('node-input-validator');

var connection = mysql.createConnection({
    host     : process.env.SQL_HOST,
    user     : process.env.SQL_USER,
    password : process.env.SQL_PASSWORD,
    database : 'Users',
    port: "3306"
});

function hashPassword(username, password){
    return new Promise( (res, rej) => {
        bcrypt.genSalt(10, function(err, salt) {
            if(err){
                console.log(err);
                rej(err);
            }
            else{
                bcrypt.hash(password, salt, function(err, hashedPassword) {
                    if(err){
                        console.log(err); 
                        rej(err);
                    }
                    else{
                        var query = `INSERT INTO Users (username,password) VALUES ('${username}','${hashedPassword}')`
                        connection.query(query, function(err, result){
                            if(err){
                                console.log(err);
                                rej(err);
                            }
                            else{
                                console.log(res);
                                res(result.insertId);
                            }
                        }) 
                    }
                });
            }
        });
    })
}

function getCredentials(userId){
    return new Promise( (res,rej) => {
        connection.query(`SELECT * FROM Users WHERE userid = ${userId}`, function(err, result){
            if(err){
                console.log(err);
                rej(err);
            }
            else{
                res(result);
            }
        })
    })
}

function registerNewUser(req, res){

    var username = req.query.username;
    var password = req.query.password;

    var inputs = {
        username: username,
        password: password
    }
    const v = new Validator(inputs, {
        username: 'required',
        password: 'required|minLength:6' 
    });

    v.check().then(async (matched) => {
        if (!matched) {
          res.status(422).send(v.errors);
        }
        else{
            try{
                var userId = await hashPassword(username,password)
                var user = await getCredentials(userId);
                var token = generateJWTToken(user[0].userid,{ username: user[0].username });
                await redis.storeCache(user[0].username, user[0].userid, token);
                res.status(200).send({ auth: true, token: token });
            }
            catch(err){
                res.status(500).send({error: err.message})
            }
        }
    })
}

router.get('/register', function(req,res){
    registerNewUser(req,res)
})

module.exports=router;
