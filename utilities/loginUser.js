var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var generateJWTToken = require('../common/generateJWTToken');
var express = require('express');
var router = express.Router();
var redis = require('../common/redisCache');
const { Validator } = require('node-input-validator');

var connectionPool = mysql.createPool({
    host     : process.env.SQL_HOST,
    user     : process.env.SQL_USER,
    password : process.env.SQL_PASSWORD,
    database : 'Users',
    port: "3306"
});

function checkConnection(req, res){

    var username = req.query.username;
    var password = req.query.password;

    var inputs = {
        username: username,
        password: password
    }
    const v = new Validator(inputs, {
        username: 'required',
        password: 'required' 
    });

    v.check().then((matched) => {
        if (!matched) {
          res.status(422).send(v.errors);
        }
        else{
            var query='SELECT * from Users where username = ?';
            connectionPool.query(query, username, function (err, result) {
                if (err){
                    console.log(err)
                    res.status(400).send({'error': err.message})
                }
                else{
                    if(result.length==0){
                        res.status(400).send({'error': 'Username is incorrect' })
                    }
                    else{
                        bcrypt.compare(password, result[0].password, function(err, isTrue){
                            if(err){
                                console.log(err)
                                res.status(400).send({'error': err.message})
                            }
                            else if(!isTrue){
                                res.status(400).send({'error': 'Incorrect Password' })
                            }
                            else{
                                var token = generateJWTToken(result[0].userid, { username: result[0].username });
                                redis.storeCache(result[0].username, result[0].userid, token).then( () => {
                                    res.status(200).send({ auth: true, token: token });
                                }).catch( (err) => {
                                    res.status(400).send({'error': err.message})
                                })
                            }
                        })
                    }
                }
            })
        }
    });
}

router.get('/login', function(req,res){
    checkConnection(req,res)
})

module.exports=router;

