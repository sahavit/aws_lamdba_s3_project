var express = require('express');
var AWS = require('aws-sdk');
var router = express.Router();

var awsConfig = { 
    "accessKeyId": process.env.ACCESS_KEY_ID,
    "secretAccessKey": process.env.SECRET_ACCESS_KEY,
    "region": process.env.REGION  
};

var s3 = new AWS.S3(awsConfig);

router.delete('/image/:fileName', async(req, res)=>{
    const value = req.params.fileName;

    const params = {
        // Bucket: 'mobiotics.image.resize', 
        Bucket: process.env.BUCKET_NAME,
        Delete: {
            Objects: [
                { Key: `${value}-1` },
                { Key: `${value}-2` },
                { Key: `${value}-3` },
                { Key: `${value}-4` }
            ], 
            Quiet: false
        }
    };
      
    await s3.deleteObjects(params, (err, data)=> {
        if (err) {
            console.log(err)
            res.status(404).json({'error': err.message})
        } 
        else{
            res.status(200).json({'success': true})
        }
    });
     
});

module.exports=router;