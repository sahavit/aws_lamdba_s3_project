var jwt = require('jsonwebtoken');
var redis = require('./redisCache')

function verifyJwtToken(req,res,next){
    if (req.headers.authorization==undefined) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }
    else{
        var token = req.headers.authorization.split(' ')[1];
        var decoded = jwt.decode(token, {complete: true})
        redis.checkCacheStorage(decoded.payload.username).then( (reply) => {
            if(!reply){
                return res.status(500).send({ error: 'You have logged out' });
            }
            else{
                var secret = reply.userid;
                jwt.verify(token, secret, function(err) {
                    if (err){
                        console.log(err)
                        return res.status(500).send({ error: 'Failed to authenticate token.' });
                    } 
                    next();
                });
            }
        }).catch((err) => {
            return res.status(401).send({ auth: false, message: err.message });
        })
    }

}

module.exports=verifyJwtToken;
