var redis = require('redis');

var redisConfig = {
    host: process.env.REDIS_HOST,
    password: process.env.REDIS_PASSWORD,
    port: process.env.REDIS_PORT
}
const redisClient = redis.createClient(redisConfig);

function storeCache(username,userid,token){
    return new Promise( (res,rej) => {
        redisClient.hmset(username,"userid",userid,"token",token,function(err, reply){
            if(err){
                console.log(err);
                rej(err);
            }
            else{
                console.log("Cache stored in redis: ",reply);
                redisClient.expire(username, 86400);
                res(reply);
            }
        });
    })
}

function checkCacheStorage(username){
    return new Promise( (res,rej) => {
        redisClient.hgetall(username,function(err,reply) {
            if(err){
                console.log(err);
                rej(err);
            }
            else{
                if(reply){
                    res(reply);
                }
                else{
                    res(false);
                }
            }
        });
    })       
}


function deleteCacheStorage(username){
    return new Promise( (res,rej) => {
        redisClient.del(username,function(err,reply) {
            if(err){
                console.log(err);
                rej(err);
            }
            else{
                if(reply===1){
                    res(true);
                }
                else{
                    res(false);
                }
            }
        });
    })
}

module.exports.storeCache = storeCache;
module.exports.checkCacheStorage = checkCacheStorage;
module.exports.deleteCacheStorage = deleteCacheStorage;
