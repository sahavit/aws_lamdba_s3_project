var jwt = require('jsonwebtoken');

function generateJWTToken(userid, payload){
    var secret = `${userid}`;
    var token = jwt.sign(payload, secret, {
        expiresIn: 86400 // expires in 24 hours
    });
    return token;
}

module.exports = generateJWTToken;