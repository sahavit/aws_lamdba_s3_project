var serverless = require('serverless-http');
var express = require('express');
var app = express();
var getObjects = require('./utilities/getObjects')
var postNewObject = require('./utilities/postNewObject')
var deleteObject = require('./utilities/deleteObject')
var loginUser = require('./utilities/loginUser');
var registerUser = require('./utilities/registerUser');
var logoutUser = require('./utilities/logoutUser')
var verifyJwtToken = require('./common/verifyJwtToken');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); 
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(loginUser);
app.use(registerUser);
app.use((req, res, next) => {
  verifyJwtToken(req,res,next);
})
app.use(logoutUser);

app.use(express.json())
app.use(getObjects)
app.use(postNewObject)
app.use(deleteObject)

module.exports.handler = serverless(app);